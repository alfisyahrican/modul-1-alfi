package apps.sen.tpmodul1alfi1202160370si40int;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "NgitungActivity";
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private Hitung mNgitung;
    private EditText mPanjang;
    private EditText mLebar;
    private TextView mHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNgitung = new Hitung();
        mHasil = findViewById(R.id.nilai);
        mPanjang = findViewById(R.id.panjang);
        mLebar = findViewById(R.id.lebar);
        Log.d(LOG_TAG,"Menghitung");
    }
    public void onHitung(View view){ compute();
    }
    @SuppressLint("SetTextI18n")
    private void compute(){
        double panjang;
        double lebar;
        try{
            panjang = getPanjang(mPanjang);
            lebar = getLebar(mLebar);
        }
        catch (NumberFormatException nfe){
            Log.e(TAG, "NumberFormatException", nfe);
            mHasil.setText("Error");
            return;
        }
        String result;
        switch (Hitung.Operator.MUL){
            case MUL:
                result=String.valueOf(mNgitung.mul(panjang, lebar));
                mHasil.setVisibility(View.VISIBLE);
                break;
            default:
                mHasil.setVisibility(View.VISIBLE);
                result="Error";
                break;
        }
        mHasil.setVisibility(Integer.parseInt(result));
        mHasil.setText(result);
    }

    private double getLebar(EditText mLebar) {
        return Double.valueOf(String.valueOf(mLebar));
    }

    private double getPanjang(EditText mPanjang) {
        return  Double.valueOf(String.valueOf(mPanjang));
    }

}
